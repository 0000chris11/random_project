Algoritmo sin_titulo
	//Realizar un diagrama de flujo que lea N numeros, 
	//calcule y escriba la suma de los pares y el producto de los impares
	
	cont=0
	acum=0
	prod=1
	Escribir "Cantidad de numeros a sumar: "
	Leer cant_num
	
	Repetir
		cont=cont+1
		si cont es par
			Escribir cont
			acum=acum+cont
			Escribir "Suma de pares: " acum
			Escribir " "
		SiNo
			Escribir cont
			prod=cont*prod
			Escribir "Producto de Impares: " prod
			Escribir " "
		FinSi
	Hasta Que cont>cant_num
FinAlgoritmo
