Algoritmo sin_titulo
	//Hacer un diagrama de flujo para sumar N primeros numeros impares
	cont=0
	impares=0
	acum=0
	Escribir "numeros impares a contar: "
	leer cant_num_imp
	
	Mientras cont<cant_num_imp hacer
		cont=cont+1
		si(impares>0)
			impares=impares+2
		sino
			impares=impares+1
		FinSi
		acum=acum+impares
		escribir impares
		escribir "suma de impares: " acum
	FinMientras
	
	Escribir " " //Espacio entre ejercicio
	
	//Hacer un diagrama de flujo para sumar N primeros numeros pares
	cont=0
	pares=0
	acum=0
	Escribir "numeros pares a contar: "
	leer cant_num_par
	
	Mientras cont<cant_num_par hacer
		cont=cont+1
		pares=pares+2
		acum=acum+pares
		Escribir pares
		Escribir "suma de pares: " acum
	FinMientras
	
	Escribir " "
	
	//Hacer un diagrama de flujo para sumar N primeros multiplos de 3
	cont=0
	acum=0
	Escribir "multiplos de 3 a contar: "
	Leer cant_num_mult3
	
	Mientras cont<cant_num_mult3 hacer
		cont=cont+1
		si cont es multiplo de 3
			Escribir cont
			acum=acum+cont
			Escribir "suma de multiplos de 3: " acum
		FinSi
	FinMientras
	
	Escribir " "
	
	//Hacer un diagrama de flujo para sumar N primeros multiplos de 5
	cont=0
	acum=0
	Escribir "multiplos de 5 a contar: "
	Leer cant_num_mult5
	
	Mientras cont<cant_num_mult5 Hacer
		cont=cont+1
		si cont es multiplo de 5
			Escribir cont
			acum=acum+cont
			Escribir "suma de multiplos de 5: " acum
		FinSi
	FinMientras
	
FinAlgoritmo
